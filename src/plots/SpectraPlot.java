package plots;


import java.awt.Color;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import model.Spectrum;
import util.Pair;


/**
 * Class to plot a spectrum.
 * @author Felix Hartkopf
 *
 */
public class SpectraPlot{ 

	/**
	 * Method to plot a spectrum.
	 * @param data A Spectrum object.
	 * @return chart of the spectrum.
	 * @author Felix Hartkopf
	 */
	public static JFreeChart createSpectraPlot(Spectrum data){

		IntervalXYDataset dataset =  createIonSeries(data.getPeaks());
		String plotTitle = "SpectraPlot for "+data.title;
		String xAxis = "m/z";
		String yAxis = "Intensity";
		ChartFactory.setChartTheme(StandardChartTheme.createLegacyTheme());
		JFreeChart chart = ChartFactory.createXYBarChart(plotTitle, xAxis, false, yAxis, dataset);

		chart.setBackgroundPaint(Color.white);
		chart.removeLegend();

		// change color
		XYPlot plot =    (XYPlot)  chart.getPlot();  
		XYBarRenderer barRndr = (XYBarRenderer) plot.getRenderer();
		BarRenderer.setDefaultBarPainter(new StandardBarPainter());
		barRndr.setSeriesPaint(0, Color.black );  
		barRndr.setShadowVisible(false);
		plot.setRenderer( barRndr);

		ChartFrame frame = new ChartFrame("SpectraPlot for "+data.title, chart);
		frame.pack();
		frame.setVisible(true);

		return chart;
	}

	public static IntervalXYDataset createIonSeries(List<Pair<Double,Double>> peaks) {
		XYSeriesCollection dataset = new XYSeriesCollection();

		XYSeries ionSeries = new XYSeries("IonSeries");

		for(Pair<Double,Double> pair: peaks){
			//System.out.println(pair.getLeft()+"\t"+pair.getRight());
			ionSeries.add(pair.getLeft(), pair.getRight());
		}



		dataset.addSeries(ionSeries);
		return dataset;
	}
}
