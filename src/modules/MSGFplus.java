package modules;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import model.ProteinLib;
import model.PSM;
import util.MSGFplusIO;
import util.MSGFplusProcess;

public class MSGFplus {

	public List<PSM> run() throws IOException{

		System.out.println("\n#######################");
		System.out.println("# Load config file... #");
		System.out.println("#######################\n");

		// Read config file
		Properties config = new Properties();
		Reader file = new FileReader("config.config");
		config.load(file);

		// Parameters
		String spectraPath = config.getProperty("spectraPath");
		String databasePath = config.getProperty("databasePath");
		String outputFile = config.getProperty("outputFile");

		// MSGFplus
		double fdr = Double.parseDouble(config.getProperty("fdr"));
		String MSGFPlusPath = config.getProperty("MSGFPlus");

		System.out.println("\n#############################");
		System.out.println("# Preprocessing database... #");
		System.out.println("#############################\n");

		ProteinLib proteinLib;

		proteinLib = new ProteinLib(databasePath);
		proteinLib.removeDuplicates();
		proteinLib.writeFasta();
		System.out.println("Proteins with duplicates: "+proteinLib.proteins.size());
		databasePath = databasePath.replace(".fasta","_noDup.fasta");

		System.out.println("\n########################");
		System.out.println("# Starting MSGFPlus... #");
		System.out.println("########################\n");

		MSGFplusProcess msgfplus = new MSGFplusProcess(databasePath,spectraPath,MSGFPlusPath,outputFile);
		//msgfplus.run();

		System.out.println("\n#################################");
		System.out.println("# Importing MSGFPlus results... #");
		System.out.println("#################################\n");

		System.out.println("Please wait... Importing MSGFplus results... ");
		List<PSM> msgfPSMs = MSGFplusIO.parseMSGFToPSMs("output/MSGFplus/"+outputFile+".tsv");

		System.out.println("Total number of imported PSMs: "+msgfPSMs.size());

		List<PSM> filteredPSMs = new ArrayList<PSM>();

		// Filter by fdr
		for(PSM tmpPSM : msgfPSMs) {
			if(tmpPSM.getQvalue()<=fdr) {
				filteredPSMs.add(tmpPSM);
			}
		}

		System.out.println("Total number of imported PSMs with FDR lower than "+fdr+": "+filteredPSMs.size());

		return filteredPSMs;


	}

}
