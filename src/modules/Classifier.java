package modules;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import smile.classification.DecisionTree;
import smile.classification.RandomForest;
import util.ProgressBar;
import smile.data.Attribute;


public class Classifier {
	/**
	 * 
	 * @param trainPath
	 * @param nTree
	 * @param balanced
	 * @return
	 * @throws IOException
	 */
	public static RandomForest trainRandomForest(String trainPath, int nTree, boolean balanced) throws IOException {


		// Create output folder
		File dir = new File("output/randomforest/");
		dir.mkdirs();

		// Progress bar
		long startTimeInput = System.currentTimeMillis();

		// Input

		System.out.println("Starting input of peaks...");
		String[] rawArray = readLines(trainPath);
		System.out.println("Completed!");

		double[][] features = new double[rawArray.length][rawArray[0].split(" ").length-2];
		String[] classes = new String[rawArray.length];


		System.out.println("Amount of raw sets: "+rawArray.length);
		System.out.println("Length of raw entries: "+rawArray[0].length());

		int index = 0;
		for(String row : rawArray) {
			String[] rowSplit = row.split(" ");
			classes[index] = rowSplit[2];
			for(int i = 0; i < rowSplit.length; i++) {
				if(!(i==2)&!(i==3)) {
					if(i<=1) {
						features[index][i] = Double.parseDouble(rowSplit[i]);
					}else {
						features[index][i-2] = Double.parseDouble(rowSplit[i]);
					}
				}
			}
			index++;
			ProgressBar.printProgress(startTimeInput, rawArray.length, index);
		}

		Collection<String> classesCol = Arrays.asList( classes);
		int aCount = Collections.frequency(classesCol , "a");
		int bCount = Collections.frequency(classesCol , "b");
		int cCount = Collections.frequency(classesCol , "c");
		int xCount = Collections.frequency(classesCol , "x");
		int yCount = Collections.frequency(classesCol , "y");
		int zCount = Collections.frequency(classesCol , "z");
		int noiseCount = Collections.frequency(classesCol , "noise");

		int[] countList = {aCount, bCount,cCount,xCount,yCount,zCount,noiseCount};

		// Balancing data
		if(balanced) {

			int minimum = countList[0];
			for(int num : countList) {
				if (num < minimum){
					minimum = num;
				}
			}		

			System.out.println(aCount+" "+bCount+" "+cCount+" "+xCount+" "+yCount+" "+zCount+" "+noiseCount);
			System.out.println("The minimum is "+minimum);
			aCount = 0;
			bCount = 0;
			cCount = 0;
			xCount = 0;
			yCount = 0;
			zCount = 0;
			noiseCount = 0;
			int skipped = 0;
			double[][] featuresBalanced = new double[minimum*7][rawArray[0].split(" ").length-2];
			String[] classesBalanced = new String[minimum*7];
			List<Integer> range = IntStream.range(0, rawArray.length).boxed().collect(Collectors.toList());
			Collections.shuffle(range);
			for(int i = 0; i<classes.length ; i++) {
				int j = range.get(i);
				switch(classes[j]){
				case "a":
					aCount++;
					if(aCount<=minimum) {
						featuresBalanced[i-skipped] = features[j];
						classesBalanced[i-skipped]  =  classes[j];	
					}else {
						skipped++;
					}
					break;
				case "b":
					bCount++;
					if(bCount<=minimum) {
						featuresBalanced[i-skipped] = features[j];
						classesBalanced[i-skipped]  =  classes[j];	
					}else {
						skipped++;
					}
					break;
				case "c":
					cCount++;
					if(cCount<=minimum) {
						featuresBalanced[i-skipped] = features[j];
						classesBalanced[i-skipped]  =  classes[j];	
					}else {
						skipped++;
					}
					break;
				case "x":
					xCount++;
					if(xCount<=minimum) {
						featuresBalanced[i-skipped] = features[j];
						classesBalanced[i-skipped]  =  classes[j];	
					}else {
						skipped++;
					}
					break;
				case "y":
					yCount++;
					if(yCount<=minimum) {
						featuresBalanced[i-skipped] = features[j];
						classesBalanced[i-skipped]  =  classes[j];	
					}else {
						skipped++;
					}
					break;
				case "z":
					zCount++;
					if(zCount<=minimum) {
						featuresBalanced[i-skipped] = features[j];
						classesBalanced[i-skipped]  =  classes[j];	
					}else {
						skipped++;
					}
					break;
				case "noise":
					noiseCount++;
					if(noiseCount<=minimum) {
						featuresBalanced[i-skipped] = features[j];
						classesBalanced[i-skipped]  =  classes[j];	
					}else {
						skipped++;
					}
					break;  
				} 
			}
			System.out.println("Completed!");
			features = featuresBalanced;
			classes = classesBalanced;
		}


		int[] classesFact = factorizeClasses(classes);

		System.out.println("Amount of feature sets: "+features.length);
		System.out.println("Amount of features: "+features[0].length);
		System.out.println("Amount of class entries: "+classes.length);


		// Train random forest
		System.out.println("Starting training of random forest...");
		//RandomForest rf = new RandomForest(features,classesFact,nTree);
		Attribute[] attributes = null;
		int maxNodes = 100;
		int nodeSize = 1; 
		double subsample = 1.0;
		int mtry = (int) Math.floor(Math.sqrt(features[0].length));
		DecisionTree.SplitRule rule = DecisionTree.SplitRule.GINI;
		int[] classWeight = countList;
		RandomForest rf = new RandomForest(attributes,
				features,
				classesFact,
				nTree,
				maxNodes,
				nodeSize,
				mtry,
				subsample,
				rule,
				classWeight);
		System.out.println("Completed!");

		return rf;
	}



	/**
	 * 
	 * @param tbcPath
	 * @param outputPath
	 * @param rf
	 * @throws IOException
	 */
	public static void classifyPeaks(String tbcPath, String outputPath, RandomForest rf) throws IOException {

		// Progress bar
		System.out.println("Starting input of peaks...");
		writeClassesPredict(rf, tbcPath, outputPath);
		System.out.println("Completed!");

	}




	/**
	 * 
	 * @param trainPath
	 * @return
	 * @throws IOException
	 */
	public static String[] readLines(String trainPath) throws IOException {
		FileReader fileReader = new FileReader(trainPath);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		List<String> lines = new ArrayList<String>();
		String line = null;
		while ((line = bufferedReader.readLine()) != null) {
			if(Character.isDigit(line.charAt(0))) {
				lines.add(line);
			}
		}
		bufferedReader.close();
		return lines.toArray(new String[lines.size()]);
	}

	/**
	 * 
	 * @param classes
	 * @return
	 */
	public static int[] factorizeClasses(String[] classes) {
		int[] classesFact = new int[classes.length];
		int index = 0;
		for(String ion : classes) {

			switch(ion){
			case "a":
				classesFact[index] = 0;
				break;
			case "b":
				classesFact[index] = 1;
				break;
			case "c":
				classesFact[index] = 2;
				break;
			case "x":
				classesFact[index] = 3;
				break;
			case "y":
				classesFact[index] = 4;
				break;
			case "z":
				classesFact[index] = 5;
				break;
			case "noise":
				classesFact[index] = 6;
				break;  
			} 
			index++;
		}
		return classesFact;
	}

	/**
	 * 
	 * @param classesFact
	 * @return
	 */
	public static String[] defactorizeClasses(int[] classesFact) {
		String[] classes = new String[classesFact.length];
		int index = 0;
		for(int ion : classesFact) {

			switch(ion){
			case 0:
				classes[index] = "a";
				break;
			case 1:
				classes[index] = "b";
				break;
			case 2:
				classes[index] = "c";
				break;
			case 3:
				classes[index] = "x";
				break;
			case 4:
				classes[index] = "y";
				break;
			case 5:
				classes[index] = "z";
				break;
			case 6:
				classes[index] = "noise";
				break;  
			} 
			index++;
		}
		return classes;
	}

	/**
	 * 
	 * @param ion
	 * @return
	 */
	public static String defactorizeClass(int ion) {

		switch(ion){
		case 0:
			return "a";
		case 1:
			return "b";
		case 2:
			return "c";
		case 3:
			return "x";
		case 4:
			return "y";
		case 5:
			return "z";
		case 6:
			return "noise";
		default:
			return "unknown"; 
		}
	}

	/**
	 * 
	 * @param tbcPath
	 * @param outputPath
	 * @param classes
	 * @throws IOException
	 */
	public static void writeClasses(String tbcPath,String outputPath, String[] classes) throws IOException {

		FileReader fileReader = new FileReader(tbcPath);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		PrintWriter writer = new PrintWriter("output/randomforest/"+outputPath+"classified.mgf", "UTF-8");
		String line = null;
		int peakNumber = 0;
		long startTimeInput = System.currentTimeMillis();

		while ((line = bufferedReader.readLine()) != null) {
			if(Character.isDigit(line.charAt(0))) {
				writer.println(line.replaceFirst("\\?", classes[peakNumber]));
				peakNumber++;
			}else {
				writer.println(line);
			}
			ProgressBar.printProgress(startTimeInput, classes.length, peakNumber+1);
		}
		writer.close();
		bufferedReader.close();
	}

	/**
	 * 
	 * @param rf
	 * @param tbcPath
	 * @param outputPath
	 * @throws IOException
	 */
	public static void writeClassesPredict(RandomForest rf,String tbcPath,String outputPath) throws IOException {

		FileReader fileReader = new FileReader(tbcPath);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		PrintWriter writer = new PrintWriter("output/randomforest/"+outputPath+"classified.mgf", "UTF-8");
		String line = null;

		while ((line = bufferedReader.readLine()) != null) {
			if(Character.isDigit(line.charAt(0))) {
				String[] rowSplit = line.split(" ");
				double[] features = new double[rowSplit.length];
				for(int i = 0; i < rowSplit.length; i++) {
					if(!(i==2)&!(i==3)) {
						if(i<=1) {
							features[i] = Double.parseDouble(rowSplit[i]);
						}else {
							features[i-2] = Double.parseDouble(rowSplit[i]);
						}
					}
				}
				int pred = rf.predict(features);
				String classResult = defactorizeClass(pred);
				writer.println(line.replaceFirst("\\?", classResult));
			}else {
				if(line.substring(0, 5).equals("SCANS")) {
					System.out.println(line);
				}
				writer.println(line);
			}
		}
		writer.close();
		bufferedReader.close();
	}


}
