package util;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.concurrent.TimeUnit;
/**
 * Class to show progress of a process
 * @author Felix Hartkopf
 *
 */
public class ProgressBar {
	public static void updateProgress(double progressPercentage) {
		final int width = 50; // progress bar width in chars
		DecimalFormat df = new DecimalFormat("#.##");

		System.out.print("\r[");
		int i = 0;
		for (; i <= (int)(progressPercentage*width); i++) {
			System.out.print(".");
		}
		for (; i < width; i++) {
			System.out.print(" ");
		}
		System.out.print("]"+df.format(progressPercentage*100)+"%");
		if(progressPercentage>=1.0){
			System.out.print("\n");
		}
	}

	public static void printProgress(long startTime, long total, long current) {
		long eta = current == 0 ? 0 : 
			(total - current) * (System.currentTimeMillis() - startTime) / current;

		String etaHms = current == 0 ? "N/A" : 
			String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(eta),
					TimeUnit.MILLISECONDS.toMinutes(eta) % TimeUnit.HOURS.toMinutes(1),
					TimeUnit.MILLISECONDS.toSeconds(eta) % TimeUnit.MINUTES.toSeconds(1));

		StringBuilder string = new StringBuilder(140);   
		int percent = (int) (current * 100 / total);
		string
		.append("\r")
		.append(String.join("", Collections.nCopies(percent == 0 ? 2 : 2 - (int) (Math.log10(percent)), " ")))
		.append(String.format(" %d%% [", percent))
		.append(String.join("", Collections.nCopies((int)Math.ceil(percent/2), "=")))
		.append('>')
		.append(String.join("", Collections.nCopies(50 - (int)Math.ceil(percent/2), " ")))
		.append(']')
		.append(String.join("", Collections.nCopies((int) (Math.log10(total)) - (int) (Math.log10(current)), " ")))
		.append(String.format(" %d/%d, ETA: %s", current, total, etaHms));

		System.out.print(string);
		if(current==total){
			System.out.print("\n");
		}
	}

}