package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Peptide {

	public String sequence;

	public List<Double> yIonList;

	public List<Double> bIonList;

	public List<Double> aIonList;

	public List<Double> xIonList;

	public List<Double> cIonList;

	public List<Double> zIonList;

	public List<Double> yIon2List;

	public List<Double> bIon2List;

	public List<Double> aIon2List;

	public List<Double> xIon2List;

	public List<Double> cIon2List;

	public List<Double> zIon2List;

	public List<Double> yIon3List;

	public List<Double> bIon3List;

	public List<Double> aIon3List;

	public List<Double> xIon3List;

	public List<Double> cIon3List;

	public List<Double> zIon3List;




	public Peptide(String sequence){
		this.sequence = sequence;
		this.yIonList = new ArrayList<Double>();
		this.bIonList = new ArrayList<Double>();
		this.aIonList = new ArrayList<Double>();
		this.xIonList = new ArrayList<Double>();
		this.cIonList = new ArrayList<Double>();
		this.zIonList = new ArrayList<Double>();

		this.yIon2List = new ArrayList<Double>();
		this.bIon2List = new ArrayList<Double>();
		this.aIon2List = new ArrayList<Double>();
		this.xIon2List = new ArrayList<Double>();
		this.cIon2List = new ArrayList<Double>();
		this.zIon2List = new ArrayList<Double>();

		this.yIon3List = new ArrayList<Double>();
		this.bIon3List = new ArrayList<Double>();
		this.aIon3List = new ArrayList<Double>();
		this.xIon3List = new ArrayList<Double>();
		this.cIon3List = new ArrayList<Double>();
		this.zIon3List = new ArrayList<Double>();

	}

	public void fragmentate(){

		for(int i = 1; i <= sequence.length(); i++){
			double bIon = Masses.calculateSequenceMass(sequence.substring(0,i))+Masses.Hydrogen;
			double yIon = Masses.Hydrogen*2 + Masses.calculateSequenceMass( sequence.substring(sequence.length()-i,sequence.length())) + Masses.Oxygen+Masses.Hydrogen;
			this.yIonList.add(yIon);
			this.yIon2List.add((yIon+Masses.Hydrogen)/2);
			this.yIon3List.add((yIon+2*Masses.Hydrogen)/3);

			this.bIonList.add(bIon);
			this.bIon2List.add((bIon+Masses.Hydrogen)/2);
			this.bIon3List.add((bIon+2*Masses.Hydrogen)/3);

			double aIon = Masses.calculateSequenceMass(sequence.substring(0,i))+Masses.Hydrogen - Masses.Carbon - Masses.Oxygen;
			double zIon =  Masses.calculateSequenceMass( sequence.substring(sequence.length()-i,sequence.length())) + 1*Masses.Oxygen - Masses.Nitrogen;
			this.aIonList.add(aIon);
			this.aIon2List.add((aIon+Masses.Hydrogen)/2);
			this.aIon3List.add((aIon+2*Masses.Hydrogen)/3);

			this.zIonList.add(zIon);
			this.zIon2List.add((zIon+Masses.Hydrogen)/2);
			this.zIon3List.add((zIon+2*Masses.Hydrogen)/3);


			if(i<sequence.length()){
				double xIon =  Masses.calculateSequenceMass( sequence.substring(sequence.length()-i,sequence.length())) + 2*Masses.Oxygen + Masses.Hydrogen + Masses.Carbon;
				double cIon = Masses.calculateSequenceMass(sequence.substring(0,i))+ 4*Masses.Hydrogen + Masses.Nitrogen;
				this.xIonList.add(xIon);
				this.xIon2List.add((xIon+Masses.Hydrogen)/2);
				this.xIon3List.add((xIon+2*Masses.Hydrogen)/3);
				this.cIonList.add(cIon);
				this.cIon2List.add((cIon+Masses.Hydrogen)/2);
				this.cIon3List.add((cIon+2*Masses.Hydrogen)/3);

			}
		}
	}


	public String classifyPeak(double mz, double tolerance){
		String ionType = "noise 0";

		// find closest per fragmentation type 
		double yIon = binarySearch(mz, this.yIonList);
		double bIon = binarySearch(mz, this.bIonList);
		double xIon = binarySearch(mz, this.xIonList);
		double zIon = binarySearch(mz, this.zIonList);
		double cIon = binarySearch(mz, this.cIonList);
		double aIon = binarySearch(mz, this.aIonList);

		double yIon2 = binarySearch(mz, this.yIon2List);
		double bIon2 = binarySearch(mz, this.bIon2List);
		double xIon2 = binarySearch(mz, this.xIon2List);
		double zIon2 = binarySearch(mz, this.zIon2List);
		double cIon2 = binarySearch(mz, this.cIon2List);
		double aIon2 = binarySearch(mz, this.aIon2List);

		double yIon3 = binarySearch(mz, this.yIon3List);
		double bIon3 = binarySearch(mz, this.bIon3List);
		double xIon3 = binarySearch(mz, this.xIon3List);
		double zIon3 = binarySearch(mz, this.zIon3List);
		double cIon3 = binarySearch(mz, this.cIon3List);
		double aIon3 = binarySearch(mz, this.aIon3List);

		List<Double> ionList = new ArrayList<Double>();
		ionList.add(Math.abs(yIon-mz));//0
		ionList.add(Math.abs(bIon-mz));//1
		ionList.add(Math.abs(xIon-mz));//2
		ionList.add(Math.abs(zIon-mz));//3
		ionList.add(Math.abs(cIon-mz));//4
		ionList.add(Math.abs(aIon-mz));//5

		ionList.add(Math.abs(yIon2-mz));//6
		ionList.add(Math.abs(bIon2-mz));//7
		ionList.add(Math.abs(xIon2-mz));//8
		ionList.add(Math.abs(zIon2-mz));//9
		ionList.add(Math.abs(cIon2-mz));//10
		ionList.add(Math.abs(aIon2-mz));//11

		ionList.add(Math.abs(yIon3-mz));//12
		ionList.add(Math.abs(bIon3-mz));//13
		ionList.add(Math.abs(xIon3-mz));//14
		ionList.add(Math.abs(zIon3-mz));//15
		ionList.add(Math.abs(cIon3-mz));//16
		ionList.add(Math.abs(aIon3-mz));//17

		int minIndex = ionList.indexOf(Collections.min(ionList));
		if(ionList.get(minIndex)<tolerance) {
			switch(minIndex){
			case 0:
				ionType = "y 1";
				return ionType;
			case 1:
				ionType = "b 1";
				return ionType;
			case 2:
				ionType = "x 1";
				return ionType;
			case 3:
				ionType = "z 1";
				return ionType;
			case 4:
				ionType = "c 1";
				return ionType;
			case 5:
				ionType = "a 1";
				return ionType;
			case 6:
				ionType = "y 2";
				return ionType;
			case 7:
				ionType = "b 2";
				return ionType;
			case 8:
				ionType = "x 2";
				return ionType;
			case 9:
				ionType = "z 2";
				return ionType;
			case 10:
				ionType = "c 2";
				return ionType;
			case 11:
				ionType = "a 2";
				return ionType;
			case 12:
				ionType = "y 3";
				return ionType;
			case 13:
				ionType = "b 3";
				return ionType;
			case 14:
				ionType = "x 3";
				return ionType;
			case 15:
				ionType = "z 3";
				return ionType;
			case 16:
				ionType = "c 3";
				return ionType;
			case 17:
				ionType = "a 3";
				return ionType;
			default:
				ionType = "noise 0";
				return ionType;
			} 	
		}else {
			return "noise 0";
		}


	}


	public static double binarySearch(double value, List<Double> list) {

		Collections.sort(list);

		if(value < list.get(0)){
			return list.get(0);
		}
		if(value > list.get(list.size()-1)){
			return list.get(list.size()-1);
		}

		int low = 0;
		int high = list.size()-1;

		while (low <= high) {
			int middle = (high + low) / 2;
			if (value < list.get(middle)) {
				high = middle - 1;
			} else if (value > list.get(middle)) {
				low = middle + 1;
			} else {
				return list.get(middle);
			}
		}
		// low == high + 1
		return (list.get(low) - value) < (value - list.get(high)) ? list.get(low) : list.get(high);
	}
}
