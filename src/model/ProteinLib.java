package model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import util.Digest;
import util.Pair;
import util.StringUtilities;



public class ProteinLib {
	/**
	 * This class holds the content of a fasta file filled with proteins.
	 *
	 * @author Felix Hartkopf
	 */
	///////////////
	// Variables //
	///////////////

	/**
	 * Path to the imported fasta file.
	 */
	public String filepath;

	/**
	 * HashMap that holds the sequences of proteins with the accession as key.
	 */
	public HashMap<String,String> proteins;

	/**
	 * HashMap that holds the sequences of proteins with the accession as key.
	 */
	public HashMap<String,List<Pair<Integer,Integer>>> peptides;

	/**
	 * HashMap that holds the header of proteins with the accession as key.
	 */
	public HashMap<String,String> header;

	/**
	 * Minimum length of a digested peptide.
	 */
	public Integer minLength;

	/**
	 * Maximum of a digested peptide.
	 */
	public Integer maxLength;

	/**
	 * Number of allowed missed cleavages.
	 */
	public Integer missedCleavages;

	//////////////////
	// Constructors //
	//////////////////

	/**
	 * Minimal constructor to build a protein library
	 * @param fastaPath
	 * @author Felix Hartkopf
	 * @throws IOException 
	 */
	public ProteinLib(String fastaPath) throws IOException {
		// Read config file
		Properties config = new Properties();
		Reader file = new FileReader("config.config");
		config.load(file);

		// Load Properties
		this.missedCleavages = Integer.parseInt(config.getProperty("missedCleavages"));
		this.minLength = Integer.parseInt(config.getProperty("minLength"));
		this.maxLength = Integer.parseInt(config.getProperty("maxLength"));

		this.filepath = fastaPath;

		this.proteins = new HashMap<String,String>();
		this.header = new HashMap<String,String>();
		this.peptides = new HashMap<String,List<Pair<Integer,Integer>>>();

		List<HashMap<String,String>> output = readFastaFile(fastaPath);
		this.proteins = output.get(0);
		this.header = output.get(1);
		this.peptides = this.digestProteins();	

	}

	/////////////////////////////
	// Getter & Setter Methods //
	/////////////////////////////

	/**
	 * Method to add a protein to the library.
	 * @param accession Accession of the protein.
	 * @param sequence Sequence of the protein.
	 * @author Felix Hartkopf
	 */
	public void addProtein(String accession, String sequence){
		proteins.put(accession, sequence);
	}

	/**
	 * Method to add a HasMap of proteins to the library.
	 * @param proteins Proteins to be added.
	 * @author Felix Hartkopf
	 */
	public void addProteins(HashMap<String,String> proteins){
		proteins.putAll(proteins);
	}

	/**
	 * Method to add a HasMap of headers to the library.
	 * @param proteins Proteins to be added.
	 * @author Felix Hartkopf
	 */
	public void addHeaders(HashMap<String,String> headers){
		header.putAll(headers);
	}

	/**
	 * Method to add a HasMap of headers to the library.
	 * @param proteins Proteins to be added.
	 * @author Felix Hartkopf
	 */
	public void addPeptides(HashMap<String,List<Pair<Integer,Integer>>> peptides){
		peptides.putAll(peptides);
	}

	/**
	 * Method to get a sequence of a protein in the library.
	 * @param accession Accession of the protein.
	 * @author Felix Hartkopf
	 * @return 
	 */
	public String getSequence(String accession){
		return proteins.get(accession);
	}

	/**
	 * Method to get all accessions of proteins in the library.
	 * @return Set of accessions.
	 * @author Felix Hartkopf
	 */
	public Set<String> getAllAccessions(){
		Set<String> accessions = proteins.keySet();
		return accessions;		
	}

	/**
	 * Method to add a header of protein to the library.
	 * @param accession Accession of the protein.
	 * @param header Header of the protein.
	 * @author Felix Hartkopf
	 */
	public void addHeader(String accession, String header){
		proteins.put(accession, header);
	}

	/**
	 * Method to get a header of protein in the library.
	 * @param accession Accession of the protein.
	 * @return Header of the protein
	 * @author Felix Hartkopf
	 */
	public String getHeader(String accession){
		return header.get(accession);
	}

	/**
	 * Method to return amount of proteins in library.
	 * @return Amount of proteins
	 * @author Felix Hartkopf
	 */
	public int size(){
		return proteins.size();
	}

	/**
	 * Method to add positions of possible peptides to library. 
	 * @param accession Accession of the protein.
	 * @param positions Positions of the peptides in the protein.
	 * @author Felix Hartkopf
	 */
	public void addPeptides(String accession, List<Pair<Integer,Integer>> positions ){
		peptides.put(accession, positions);
	}

	/**
	 * Method to get all positions of peptides in library of one protein. 
	 * @param accession Accession of the protein.
	 * @author Felix Hartkopf
	 * @return List of Pair that are the start and the end of a peptide
	 */
	public List<Pair<Integer, Integer>> getPeptides(String accession){		
		return peptides.get(accession);
	}

	/**
	 * Method to convert a pair in actual amino acid sequence.
	 * @param accession Accession of the protein.
	 * @param pair Pair of the start and the end of the peptide.
	 * @author Felix Hartkopf
	 * @return The sequence of the peptide.
	 */
	public String pairToString(String accession, Pair<Integer, Integer> pair){	
		String sequence = proteins.get(accession).substring(pair.getLeft(), pair.getRight());
		return sequence;
	}

	///////////////////
	// Class Methods //
	///////////////////

	/**
	 * Method to digest all proteins in the library and store peptides.
	 * 
	 * @author Felix Hartkopf
	 */
	public HashMap<String,List<Pair<Integer,Integer>>> digestProteins(){

		System.out.println("Digesting proteins...");		

		// start record time
		long startTime = System.currentTimeMillis();

		HashMap<String,List<Pair<Integer,Integer>>> peptides = new HashMap<String,List<Pair<Integer,Integer>>>();

		// Iterate through proteins
		HashMap<String,String> proteins = this.proteins;
		Iterator<Entry<String, String>> iterator = proteins.entrySet().iterator();
		int countPeptides = 0;

		while (iterator.hasNext()) {
			Map.Entry<String, String> entry = (Map.Entry<String, String>)iterator.next();
			String accession = entry.getKey();
			String sequence = entry.getValue();

			// Digest protein
			List<String> peptidesList = Digest.performTrypticCleavage(sequence, this.minLength, this.maxLength, this.missedCleavages);

			// Get start and end positions
			List<Pair<Integer,Integer>> positions = new ArrayList<Pair<Integer,Integer>>();
			for(String peptide :peptidesList){
				Integer start = sequence.indexOf(peptide);
				Integer end = start + peptide.length()-1;
				Pair<Integer,Integer> pair = new Pair<Integer,Integer>(start,end);
				positions.add(pair);
			}

			peptides.put(accession, positions);
			countPeptides = positions.size() + countPeptides;

		}

		// debug output
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		System.out.println("Total time for digesting proteins: "+StringUtilities.millisToShortDHMS(elapsedTime)+" seconds");		
		System.out.println("Peptides stored in protein library: "+countPeptides);	 
		return peptides; 
	}

	/**
	 * Method to remove protein duplicates from ProteinLib
	 * 
	 * @author Felix Hartkopf
	 */
	public void removeDuplicates(){
		Set<String> keys = this.proteins.keySet();
		for(String key : keys){
			for(String seqkey : keys){
				if(key!=seqkey){
					if(this.proteins.get(seqkey).equals(this.proteins.get(key))){
						this.peptides.remove(seqkey);
						this.header.remove(seqkey);
						this.peptides.remove(seqkey);
					}
				}			
			}
		}

	}

	/**
	 * Method to write ProteinLib to a fasta file.
	 * @param filePath
	 * @author Felix Hartkopf
	 */
	public void writeFasta(){
		String output = this.filepath.replace(".fasta","_noDup.fasta");
		Set<String> keys = this.peptides.keySet();
		Path file = Paths.get(output);
		List<String> lines = new ArrayList<String>() ;

		// Create lines 
		for(String key : keys){
			lines.add(header.get(key));
			List<String> plitSeq = new ArrayList<String>(Arrays.asList(proteins.get(key).split("(?<=\\G.{60})")));
			lines.addAll(plitSeq);
		}

		// Write lines to File
		try {
			Files.write(file, lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			System.out.print("Can�t write to file "+output+".");
			e.printStackTrace();
		}

	}

	/**
	 * Reads fasta file and stores proteins in library
	 * @param fastaPath
	 * @author Felix Hartkopf
	 */
	public List<HashMap<String, String>> readFastaFile(String fastaPath){

		System.out.println("Importing proteins...");		
		// start record time
		long startTime = System.currentTimeMillis();

		HashMap<String,String> proteins = new HashMap<String,String>();
		HashMap<String,String> headers = new HashMap<String,String>();

		try {
			FileReader fastaFile;

			fastaFile = new FileReader(fastaPath);
			BufferedReader in = new BufferedReader(fastaFile);

			String line;
			String header = null;

			// one loop is one protein
			while((line=in.readLine())!=null){ 

				String sequence = "";

				// header of protein
				if(line.startsWith(">")){ 
					header = line;
				}

				// sequence of protein
				while(!line.startsWith(">")){ 
					sequence = sequence + line;
					line=in.readLine();
					if(line==null){
						break;
					}
				}

				// Get accession
				String accession = header;
				String[] splitHeader = header.split(Pattern.quote("|"));
				if(splitHeader.length==3){
					accession = splitHeader[1];
				}

				// Get header
				headers.put(accession,header);

				// Add protein to library
				proteins.put(accession, sequence);	

				// next header
				if(line!=null&&line.startsWith(">")){ 
					header = line;
				}	
			}

			in.close();

		} catch (FileNotFoundException e) {
			System.out.println("Fasta file not found. Please check path.");		
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Error while reading document.");		
			e.printStackTrace();
		}

		// debug output
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		System.out.println("Total time for importing proteins: "+StringUtilities.millisToShortDHMS(elapsedTime)+" seconds");		
		System.out.println("Proteins stored in protein library: "+proteins.size());
		List<HashMap<String,String>> output = new ArrayList<HashMap<String,String>>();
		output.add(proteins);
		output.add(headers);
		return output;		

	}

	////////////////////
	// Static Methods //
	////////////////////

}
